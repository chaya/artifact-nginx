#Description
* This image will download a config file and start nginx.
* You can specify an $ARTIFACT_USERNAME/$ARTIFACT_PASSWORD to access $ARTIFACT_URL if needed (Basic http authentication).

#Usages:
* $ARTIFACT_URL=http://artifactory.arxcf.com/artifactory/web-snapshot-local/com/arx/origination-front/2.2.8-SNAPSHOT/origination-front-feature-sprint-36-2.2.8-SNAPSHOT.zip
* $ARTIFACT_USERNAME=username (optional)
* $ARTIFACT_PASSWORD=password (optional)

#Build:
docker build -t artifact-nginx .

#Run:
docker run \
-e ARTIFACT_USERNAME=admin \
-e ARTIFACT_PASSWORD=AP91GW2VoPT6CyzF3za9tYNcfY1 \
-e ARTIFACT_URL=http://artifactory.arxcf.com/artifactory/web-snapshot-local/com/arx/origination-front/2.2.8-SNAPSHOT/origination-front-feature-sprint-36-2.2.8-SNAPSHOT.zip \
-e NGINX_ADDITIONNAL_CONF="location /oauth/ { set \$upstream1 "http://oauth-api:8081/oauth/"; proxy_pass \$upstream1; } \nlocation /services/ { set \$upstream2 "http://ntw-api:8080/services/"; proxy_pass \$upstream2; } \nlocation /admin/monitoring { set \$upstream3 "http://ntw-api:8080/admin/monitoring"; proxy_pass \$upstream3; } \nlocation /crm/financiere_monceau/ { set \$upstream4 "http://crm-api:8082/crm/financiere_monceau"; proxy_pass \$upstream4; }" \
--name network-web \
-p 80:80 \
-d network-web

#Tips
This image use the docker resolver and variable to enable proxy_pass even if the hosts are not reachable.

