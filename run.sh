#!/bin/bash

set -e

APP_PATH=/tmp/app.zip

if [ -e "$APP_PATH" ]; then
    echo 'App already installed, skipping installation'
else
    echo "ARTIFACT_URL: $ARTIFACT_URL"
    echo "ARTIFACT_USERNAME: $ARTIFACT_USERNAME"
    echo "ARTIFACT_PASSWORD: $ARTIFACT_PASSWORD"

    AUTHORIZATION=""
    if [ -z "$ARTIFACT_USERNAME" ]; then
        echo 'No credentials found'
    else
        echo 'Using credentials ' ${ARTIFACT_USERNAME}':*'
        AUTHORIZATION="-u $ARTIFACT_USERNAME:$ARTIFACT_PASSWORD"
    fi

    echo 'Downloading artifact at' ${ARTIFACT_URL} 'using authorization:' ${AUTHORIZATION}
    curl $AUTHORIZATION $ARTIFACT_URL --output $APP_PATH

    echo 'Installing artifact'
    rm -rf /usr/share/nginx/html/*
    unzip $APP_PATH -d /usr/share/nginx/html/

    echo 'Adding reverse proxies for Api & Oauth'
    cat > /etc/nginx/nginx.conf <<EOF
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;
    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       80;
        resolver 127.0.0.11 valid=30s;

        location / {
            root   /usr/share/nginx/html;
            index  index.html;
        }
EOF
    echo -e "$NGINX_ADDITIONNAL_CONF" >> /etc/nginx/nginx.conf
    cat >> /etc/nginx/nginx.conf <<EOF
    }
}
EOF
fi
more /etc/nginx/nginx.conf
echo 'Launching Nginx'
nginx -g 'daemon off;'