FROM nginx
MAINTAINER chaya

RUN apt-get update && apt-get install -y curl unzip

COPY run.sh /run.sh
RUN chmod +x /run.sh

CMD ["/run.sh"]